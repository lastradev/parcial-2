package com.example.parcial2.models

data class MenuTile(
    var id: Int,
    var image: Int,
    var name: String
)
