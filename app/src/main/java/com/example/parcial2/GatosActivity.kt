package com.example.parcial2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.widget.EditText
import com.google.android.material.floatingactionbutton.FloatingActionButton

class GatosActivity : AppCompatActivity() {
    private lateinit var etOwnerName: EditText
    private lateinit var etCatName: EditText
    private lateinit var etCatAge: EditText
    private lateinit var btSave: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gatos)

        etOwnerName = findViewById(R.id.etOwnerName)
        etCatName = findViewById(R.id.etCatName)
        etCatAge = findViewById(R.id.etCatAge)

        btSave = findViewById(R.id.btSave)

        btSave.setOnClickListener {
            savePreferences()
        }

       // restoreForm()
    }

    private fun savePreferences() {
        val ownerName = etOwnerName.text.toString()
        val catName = etCatName.text.toString()
        val catAge = etCatAge.text.toString().toInt()

        val formPreferences = getSharedPreferences("MAIN_FORM", MODE_PRIVATE)
        val editor = formPreferences.edit()

        editor.putString("owner_name", ownerName)
        editor.putString("cat_name", catName)
        editor.putInt("cat_age", catAge)
        editor.apply()
    }

    private fun restoreForm() {
        val formPreferences = getSharedPreferences("MAIN_FORM", MODE_PRIVATE)

        val ownerName = formPreferences.getString("owner_name", "")
        val catName = formPreferences.getString("cat_name", "")
        val catAge = formPreferences.getInt("cat_age", 0)

        etOwnerName.setText(ownerName)
        etCatName.setText(catName)
        etCatAge.setText(catAge)
    }

}