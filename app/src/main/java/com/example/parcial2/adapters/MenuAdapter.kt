package com.example.parcial2.adapters

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.example.parcial2.GatosActivity
import com.example.parcial2.R
import com.example.parcial2.models.MenuTile
import java.security.AccessController.getContext


const val CLOSE_TILE_ID = 4
const val CAT_TILE_ID = 1

class MenuAdapter(menuTiles: ArrayList<MenuTile>, context: Context) : RecyclerView.Adapter<MenuAdapter.ViewContainer> () {
    private var innerMenuTiles: ArrayList<MenuTile> = menuTiles
    var innerContext: Context = context

    inner class ViewContainer(view: View) :
        RecyclerView.ViewHolder(view){

        var ivTile : ImageView
        var tvName: TextView

        init {
            ivTile = view.findViewById(R.id.ivMenuTile)
            tvName = view.findViewById(R.id.tvMenuTileName)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewContainer {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.menu_tile, parent, false)

        return ViewContainer(view)
    }

    override fun onBindViewHolder(holder: ViewContainer, position: Int) {
        val menuTile: MenuTile = innerMenuTiles[position]
        holder.ivTile.setImageResource(menuTile.image)
        holder.tvName.text = menuTile.name

        if (menuTile.id == CLOSE_TILE_ID) {
            holder.ivTile.setOnClickListener {
                val activity = innerContext as Activity
                activity.finish()
            }
        }

        if(menuTile.id == CAT_TILE_ID){
            holder.ivTile.setOnClickListener {
                val intent = Intent(innerContext, GatosActivity::class.java)

                // No se que sea esta mounstruosidad pero funciona :D
                val activity = innerContext as Activity
                activity.startActivity(intent)
            }
        }
    }

    override fun getItemCount(): Int {
        return innerMenuTiles.size
    }
}
