package com.example.parcial2

import android.content.Context
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.parcial2.adapters.MenuAdapter
import com.example.parcial2.models.MenuTile

const val COLUMNS_QUANTITY = 2

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rvMenu: RecyclerView = findViewById(R.id.recycler_view)

        val menuTiles = arrayListOf(
            MenuTile(1, R.drawable.cat, "Gatos"),
            MenuTile(2, R.drawable.profile, "Perfil"),
            MenuTile(3, R.drawable.config, "Configurar"),
            MenuTile(4, R.drawable.close, "Cerrar"),
        )

        val layoutAdministrator = GridLayoutManager(this, COLUMNS_QUANTITY)

        rvMenu.layoutManager = layoutAdministrator
        rvMenu.adapter = MenuAdapter(menuTiles, this)
    }

    override fun onCreateOptionsMenu(menu: Menu?):Boolean {
        menuInflater.inflate(R.menu.menu, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.wifi) {
            val connectivityManager = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

            if(connectivityManager.activeNetwork != null){
                Toast.makeText(this, "Connected", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(this, "Disconnected", Toast.LENGTH_SHORT).show()
            }
        }
        return super.onOptionsItemSelected(item)
    }
}